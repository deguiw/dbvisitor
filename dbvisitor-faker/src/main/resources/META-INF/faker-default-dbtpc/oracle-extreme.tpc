# see https://docs.oracle.com/en/database/oracle/oracle-database/21/sqlrf/Data-Types.html

define oracle {
    priority     = 1
    policy       = "extreme"
    policyDesc   = "Oracle TypeProcessor Policy(Extreme)"
}

[binary_float]
[float]
    jdbcType       = float
    seedType       = number
    numberType     = decimal
    minMax        += range(10, -1.17549E-38        , -3.40282E+38)
    minMax        += range(10, 1.17549E-38         , 3.40282E+38)
    minMax        += range(30, -999999999.999999999, 999999999.999999999)
    minMax        += range(30, -0.999999999        , 0.999999999)
    precision      = ${columnSize}
    scale          = ${decimalDigits}

[binary_double]
    jdbcType       = double
    seedType       = number
    numberType     = decimal
    minMax        += range(10, -2.22507485850720E-308, -1.79769313486231E+308)
    minMax        += range(10, 2.22507485850720E-308 , 1.79769313486231E+308)
    minMax        += range(30, -999999999.999999999, 999999999.999999999)
    minMax        += range(30, -0.999999999        , 0.999999999)
    precision      = ${columnSize}
    scale          = ${decimalDigits}

[number]
    jdbcType       = decimal
    seedType       = number
    numberType     = decimal
    precision      = ifThen(oraTestNumber(), ${columnSize}, 40)
    scale          = ifThen(oraTestNumber(), ${decimalDigits}, 20)

[char]
[nchar]
[varchar2]
[nvarchar2]
    jdbcType       = varchar
    seedType       = string
    minLength      = 1
    maxLength      = safeMaxLength(1, ${columnSize}, 1000, 10)
    characterSet   = [ letter_number ]
    allowEmpty     = false

[clob]
[nclob]
    jdbcType       = varchar
    seedType       = string
    minLength      = 1
    maxLength      = safeMaxLength(1, ${columnSize}, 1000, 10)
    characterSet   = [ letter_number ]
    allowEmpty     = false
    ignoreAct      = [ DeleteWhere, UpdateWhere ]

[long]
    jdbcType       = varchar
    seedType       = string
    minLength      = 1
    maxLength      = safeMaxLength(1, ${columnSize}, 1000, 10)
    characterSet   = [ letter_number ]
    allowEmpty     = false
    ignoreAct      = [ DeleteWhere ]

[blob]
[raw]
["long raw"]
    jdbcType       = blob
    seedType       = bytes
    minLength      = 0
    maxLength      = safeMaxLength(0, ${columnSize}, 4096, 10)
    ignoreAct      = [ DeleteWhere, UpdateWhere ]

[rowid]
[urowid]
    jdbcType       = varchar
    seedType       = string
    allowEmpty     = false
    characterSet   = [ letter_number ]
    ignoreAct      = [ UpdateSet, Insert ]

[date]
    jdbcType       = date
    seedType       = date
    dateType       = LocalDate
    genType        = random
    rangeForm      = "0001-01-01"
    rangeTo        = "9999-12-31"

[timestamp]
    jdbcType       = timestamp
    seedType       = date
    dateType       = LocalDateTime
    genType        = random
    precision      = safeMaxLength(0, ${decimalDigits}, 9, 3)
    rangeForm      = "0001-01-01 00:00:00.000000000"
    rangeTo        = "9999-12-31 00:59:59.999999999"

["timestamp with local time zone"]
["timestamp with time zone"]
    # https://www.iana.org/time-zones
    jdbcType       = timestamp_with_timezone
    seedType       = date
    dateType       = OffsetDateTime
    genType        = random
    precision      = safeMaxLength(0, ${decimalDigits}, 9, 3)
    rangeForm      = "0001-01-01 00:00:00.000000000"
    rangeTo        = "9999-12-31 00:59:59.999999999"
    zoneForm       = "-14:00"
    zoneTo         = "+14:00"

["interval year to month"]
["interval day to second"]
[xmltype]
[bfile]
    throw "this columnType Unsupported."